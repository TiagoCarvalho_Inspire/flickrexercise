//
//  ExerciseTests.swift
//  ExerciseTests
//
//  Created by Tiago on 17/08/2019.
//  Copyright © 2019 Tiago. All rights reserved.
//

import XCTest
@testable import Exercise

class ExerciseTests: XCTestCase {
    
    override func setUp() {
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }

    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }
    
    func testAlbumResponse() throws {
         guard
             let path = Bundle(for: type(of: self)).path(forResource: "AlbumResponse", ofType: "json")
             else { fatalError("Can't find AlbumResponse.json file") }
         
         let data = try Data(contentsOf: URL(fileURLWithPath: path))
         let response = try JSONDecoder().decode(AlbumResponse.self, from: data)

        XCTAssert((response as Any) is AlbumResponse)
        XCTAssertEqual(response.stat, "ok")
        
        let album = response.photos
        
        XCTAssert((album as Any) is Album)

        let photos = response.photos.photo

        XCTAssert((photos as Any) is [Photo])
        
        let photo = response.photos.photo.first

        XCTAssertEqual(photo?.id, "48878153927")
        XCTAssertEqual(photo?.title, "super duty. 2018.")
    
     }
    
    
    func testUserResponse() throws {
        guard
            let path = Bundle(for: type(of: self)).path(forResource: "UserResponse", ofType: "json")
            else { fatalError("Can't find UserResponse.json file") }
        
        let data = try Data(contentsOf: URL(fileURLWithPath: path))
        let response = try JSONDecoder().decode(UserResponse.self, from: data)

        XCTAssert((response as Any) is UserResponse)
        XCTAssertEqual(response.stat, "ok")

        let user = response.user

        XCTAssert((user as Any) is User)
        XCTAssertEqual(user?.id, "49191827@N00")
        XCTAssertEqual(user?.nsid, "49191827@N00")

        let username = response.user?.username

        XCTAssert((username as Any) is Username)
        XCTAssertEqual(username?._content, "eyetwist")
   
    }

    func testPerformanceGetUserId() {
        self.measure {
            FlickrAPIManager.sharedInstance.getUserIdWithUsername(username: "eyetwist", onSuccess: { response in
                print(response)
                
            }, onFailure: { error in
                print(error)
            })
        }
    }

}
