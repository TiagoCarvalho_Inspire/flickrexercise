//
//  ViewController.swift
//  Exercise
//
//  Created by Tiago on 17/08/2019.
//  Copyright © 2019 Tiago. All rights reserved.
//

import UIKit
import Foundation

class RootViewController: UIViewController {
    
    @IBOutlet weak var searchField: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
    }

    @IBAction func getUserId(_ sender: Any) {
        
        let search: String = searchField.text ?? ""
        FlickrAPIManager.sharedInstance.getUserIdWithUsername(username: search, onSuccess: { response in
            
            self.responseHandler(response:response)
            
        }, onFailure: { error in
                ExerciseHelper.showAlert(message:error.localizedDescription, context:self)
        })
    }
    
    func responseHandler (response:UserResponse) {
        
        if let message = response.message {
           ExerciseHelper.showAlert(message: message, context:self)
            return;
        }
        
        if let user = response.user {
            DispatchQueue.main.async {
                self.performSegue(withIdentifier: "photoTableSegue", sender:user.id)
            }
        }
    }

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        let ptvc = segue.destination as! PhotoTableViewController
        ptvc.userId = sender as! String
    }
    
}

