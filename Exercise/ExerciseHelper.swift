//
//  ExerciseHelper.swift
//  Exercise
//
//  Created by Tiago Carvalho on 04/10/2019.
//  Copyright © 2019 Tiago. All rights reserved.
//

import Foundation
import UIKit

class ExerciseHelper {
    
    static func showAlert (message:String, context:UIViewController) {
        let alert = UIAlertController(title: "Alert", message: message, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
        DispatchQueue.main.async {
            context.present(alert, animated: true, completion: nil)
        }
    }
}
