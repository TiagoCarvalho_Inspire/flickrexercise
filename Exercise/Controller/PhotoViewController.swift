//
//  PhotoViewController.swift
//  Exercise
//
//  Created by Tiago Carvalho on 23/08/2019.
//  Copyright © 2019 Tiago. All rights reserved.
//

import UIKit

class PhotoViewController: UIViewController {
    
    var photoId = String()
    
    @IBOutlet weak var imageView: UIImageView!
    
    @IBOutlet weak var descriptionLabel: UILabel!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var locationLabel: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.showImageInfo()
        self.showImageView()
        
    }
    
    func showImageView() {
        FlickrAPIManager.sharedInstance.getPhotoSizes(id: photoId, onSuccess: { response in
            DispatchQueue.main.async {
                if let url = NSURL(string:self.getImageURL(response: response)) {
                    if let data = try? Data(contentsOf: url as URL) {
                        if let image = UIImage(data:data) {
                            self.imageView.image = image
                        }
                    }
                }
            }
        }, onFailure: { error in
            ExerciseHelper.showAlert(message:error.localizedDescription, context:self)
        })
        
    }
    
    func showImageInfo() {
        
        FlickrAPIManager.sharedInstance.getPhotoInfo(id: photoId, onSuccess: { response in
            DispatchQueue.main.async {

                self.titleLabel.text = response.photo.title._content
                self.descriptionLabel.text = response.photo.description._content
                //self.locationLabel.text = ""
                            
            }
        }, onFailure: { error in
            ExerciseHelper.showAlert(message:error.localizedDescription, context:self)
        })
    }
    
    func getImageURL(response:PhotoSizesResponse) -> String {
        
        let photo = response.sizes.size.filter({ return $0.label == "Medium"})
        let url = photo[0].source

       return url;
    }
    
}
