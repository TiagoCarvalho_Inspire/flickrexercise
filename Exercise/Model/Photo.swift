//
//  Photo.swift
//  Exercise
//
//  Created by Tiago Carvalho on 10/10/2019.
//  Copyright © 2019 Tiago. All rights reserved.
//

import Foundation

struct Photo: Codable {
    let id: String
    let title: String
}

struct PhotoInfo: Codable {
    let description: Description
    let title: Title
}

struct PhotoSizes: Codable {
    let size: [Size]
}

struct Description: Codable {
    let _content:String
}

struct Title: Codable {
    let _content:String
}

struct Size: Codable {
    let height: Int
    let label: String
    let media: String
    let source: String
    let url: String
    let width: Int
}

struct PhotoInfoResponse: Codable {
    let photo: PhotoInfo
    let stat: String
}

struct PhotoSizesResponse: Codable {
    let stat: String
    let sizes: PhotoSizes
}



