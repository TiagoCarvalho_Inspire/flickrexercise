//
//  User.swift
//  Exercise
//
//  Created by Tiago Carvalho on 10/10/2019.
//  Copyright © 2019 Tiago. All rights reserved.
//

import Foundation

struct User: Codable {
    let id:String
    let nsid:String
    let username:Username
}

struct Username: Codable {
    let _content: String
}

struct UserResponse: Codable {
    let message:String?
    let stat:String
    let user:User?
}

