//
//  FlickrAPIManager.swift
//  Exercise
//
//  Created by Tiago on 18/08/2019.
//  Copyright © 2019 Tiago. All rights reserved.
//

import Foundation

class FlickrAPIManager {
    
    let baseURL = "https://api.flickr.com/services/rest/"
    let FlickrAPIKey = "e50b39fd3d636c02c1756ed506200fdc"
    static let sharedInstance = FlickrAPIManager()
    
    func getUserIdWithUsername(username: String, onSuccess: @escaping(UserResponse) -> Void, onFailure: @escaping(Error) -> Void){
        
        let encodedUsername = username.addingPercentEncoding(withAllowedCharacters: .alphanumerics)
        
        let url : String = baseURL + "?&method=flickr.people.findByUsername&api_key=" + FlickrAPIKey + "&username=\( encodedUsername!)&format=json&nojsoncallback=1"
        let request: NSMutableURLRequest = NSMutableURLRequest(url: NSURL(string: url)! as URL)
        request.httpMethod = "GET"
        let session = URLSession.shared
        let task = session.dataTask(with: request as URLRequest, completionHandler: {data, _, error -> Void in
            if(error != nil){
                onFailure(error!)
            } else{
                let response = try? JSONDecoder().decode(UserResponse.self, from:data!)
                onSuccess(response!)
            }
        })
        task.resume()
    }
    
    func getPublicPhotos(id: String, page: Int, pageSize: Int, onSuccess: @escaping(AlbumResponse) -> Void, onFailure: @escaping(Error) -> Void){
            
        let url : String = baseURL + "?&method=flickr.people.getPublicPhotos&api_key=" + FlickrAPIKey + "&user_id=" + id +
            "&per_page=" + String(pageSize) + "&page=" + String(page) + "&format=json&nojsoncallback=1"
        let request: NSMutableURLRequest = NSMutableURLRequest(url: NSURL(string: url)! as URL)
        request.httpMethod = "GET"
        let session = URLSession.shared
        let task = session.dataTask(with: request as URLRequest, completionHandler: {data, _, error -> Void in
            if(error != nil){
                onFailure(error!)
            } else{
                let response = try? JSONDecoder().decode(AlbumResponse.self, from:data!)
                onSuccess(response!)
            }
        })
        task.resume()
    }
    
    func getPhotoInfo(id: String, onSuccess: @escaping(PhotoInfoResponse) -> Void, onFailure: @escaping(Error) -> Void){
        
        let url : String = baseURL + "?&method=flickr.photos.getInfo&api_key=" + FlickrAPIKey + "&photo_id=" + id +
        "&format=json&nojsoncallback=1"
        let request: NSMutableURLRequest = NSMutableURLRequest(url: NSURL(string: url)! as URL)
        request.httpMethod = "GET"
        let session = URLSession.shared
        let task = session.dataTask(with: request as URLRequest, completionHandler: {data, _, error -> Void in
            if(error != nil){
                onFailure(error!)
            } else{
                let response = try? JSONDecoder().decode(PhotoInfoResponse.self, from:data!)
                onSuccess(response!)
            }
        })
        task.resume()
    }
    
    func getPhotoSizes(id: String, onSuccess: @escaping(PhotoSizesResponse) -> Void, onFailure: @escaping(Error) -> Void){
        
        let url : String = baseURL + "?&method=flickr.photos.getSizes&api_key=" + FlickrAPIKey + "&photo_id=" + id +
        "&format=json&nojsoncallback=1"
        let request: NSMutableURLRequest = NSMutableURLRequest(url: NSURL(string: url)! as URL)
        request.httpMethod = "GET"
        let session = URLSession.shared
        let task = session.dataTask(with: request as URLRequest, completionHandler: {data, _, error -> Void in
            if(error != nil){
                onFailure(error!)
            } else{
                let response = try? JSONDecoder().decode(PhotoSizesResponse.self, from:data!)
                onSuccess(response!)
            }
        })
        task.resume()
    }
    
}
