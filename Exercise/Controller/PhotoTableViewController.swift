//
//  PhotoTableViewController.swift
//  Exercise
//
//  Created by Tiago on 18/08/2019.
//  Copyright © 2019 Tiago. All rights reserved.
//

import UIKit
import PaginatedTableView

class PhotoTableViewController: UIViewController {
    
   // var pageNumber = 1
    var list = [Photo]()
    var userId = String()

    @IBOutlet weak var tableView: PaginatedTableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Add paginated delegates only
        tableView.paginatedDelegate = self
        tableView.paginatedDataSource = self
        
        // More settings
        tableView.enablePullToRefresh = true
        tableView.loadData(refresh: true)

    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        let pvc = segue.destination as! PhotoViewController
        pvc.photoId = sender as! String
    }

}

extension PhotoTableViewController: PaginatedTableViewDelegate {
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 100
    }
    
    func loadMore(_ pageNumber: Int, _ pageSize: Int, onSuccess: ((Bool) -> Void)?, onError: ((Error) -> Void)?) {
        FlickrAPIManager.sharedInstance.getPublicPhotos(id: userId, page:pageNumber, pageSize:pageSize, onSuccess: { response in
            DispatchQueue.main.async {
                                                    
                for photo in response.photos.photo {
                    self.list.append(photo)
                }
                    
                if self.list.count == 0 {
                    ExerciseHelper.showAlert(message: "No public images found", context: self)
                }
                
                onSuccess?(true)
        
            }
        }, onFailure: { error in
            ExerciseHelper.showAlert(message:error.localizedDescription, context: self)
        })
    }
}

extension PhotoTableViewController: PaginatedTableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return list.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "PhotoTableViewCell", for: indexPath) as? PhotoTableViewCell else {
            fatalError("The dequeued cell is not an instance of PhotoTableViewCell.")
        }
        cell.label.text = String(describing: self.list[indexPath.row].title)
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let photoId = String(describing: self.list[indexPath.row].id)
        performSegue(withIdentifier: "photoSegue", sender:photoId)
        
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
}
