//
//  AlbumResponse.swift
//  Exercise
//
//  Created by Tiago Carvalho on 10/10/2019.
//  Copyright © 2019 Tiago. All rights reserved.
//

import Foundation

struct Album: Codable {
    let photo:[Photo]
}

struct AlbumResponse: Codable {
    let photos:Album
    let stat:String
}


